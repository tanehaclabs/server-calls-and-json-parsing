package com.example.taneha.assignment5.activities;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.TextView;
import android.widget.Toast;

import com.example.taneha.assignment5.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;


public class Display extends ActionBarActivity {
    String myId;
    TextView firstName;
    TextView lastName;
    TextView genderView;
    TextView linkView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);
        firstName = (TextView) findViewById(R.id.first_name_show);
        lastName = (TextView) findViewById(R.id.last_name_show);
        genderView = (TextView) findViewById(R.id.gender_show);
        linkView = (TextView) findViewById(R.id.link_show);


        myId = getIntent().getStringExtra("id");
        String url2 = "https://graph.facebook.com/" + myId + "?access_token=CAACEdEose0cBAHZAS9pDZCxkITqRLHNLzORhZC7oQx7se9lPWP3qqq7bi5WZBamsYZBpmwGfk1oZCVRTZBTZACv16N1XOIbahMshPZABeqAkZB4KXdH1jABzKvwnsTZASBIfLtKzbfZB27HmsUZBFRMEk3xXE5eZAE36HcGR9RaaYFREI5BIukTN5csGE7dYUPUW0n7x7j1vbYJKn51WX98lF0BXYFZBrMheo473fYZD";

        AsyncHttpClient client = new AsyncHttpClient();
        client.get(Display.this, url2, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(String s) {
                        super.onSuccess(s);
                        try {
                            JSONObject obj = new JSONObject(s);
                            String first_name = obj.getString("first_name");
                            String last_name = obj.getString("last_name");
                            String gender = obj.getString("gender");
                            String link2 = obj.getString("link");
                            firstName.setText(first_name);
                            lastName.setText(last_name);
                            genderView.setText(gender);
                            linkView.setText(link2);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Throwable throwable, String s) {
                        super.onFailure(throwable, s);
                        Toast.makeText(Display.this, "Execution failed!", Toast.LENGTH_LONG).show();

                    }

                }


        );


    }
}






