package com.example.taneha.assignment5.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.taneha.assignment5.R;
import com.example.taneha.assignment5.activities.Display;
import com.example.taneha.assignment5.utils.FacebookUtils;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends ActionBarActivity {
    ListView listView;
    ArrayAdapter listAdapter;
    final List<String> friendList = new ArrayList<>();
    final List<String> idList = new ArrayList<>();
    String url = "https://graph.facebook.com/me/friends?access_token=";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.list_item);
        FacebookUtils.fbLogin(0, MainActivity.this);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(MainActivity.this, Display.class);
                String idPass = idList.get(position);

                intent.putExtra("id", idPass);

                startActivity(intent);

            }
        });


    }

    public void getFriends(String fbAccessToken) {
        String tokenUrl = url + fbAccessToken;
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(MainActivity.this, tokenUrl, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(String s) {
                        super.onSuccess(s);
                        try {
                            JSONObject obj = new JSONObject(s);
                            JSONArray friends = obj.getJSONArray("data");
                            for (int i = 0; i < friends.length(); i++) {
                                String name = friends.getJSONObject(i).getString("name");
                                friendList.add(name);
                                String id = friends.getJSONObject(i).getString("id");
                                idList.add(id);
                            }
                            listAdapter = new ArrayAdapter(MainActivity.this, android.R.layout.simple_list_item_1, friendList);
                            listView.setAdapter(listAdapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Throwable throwable, String s) {
                        super.onFailure(throwable, s);
                        Toast.makeText(MainActivity.this, "Sorry Try Again Later!", Toast.LENGTH_LONG).show();
                    }
                }
        );
    }
}